using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Utilities;

namespace Pruebas
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestExcelGenerator()
        {

            List<string> datos = new List<string>();

            datos.Add("ejemplo1");
            datos.Add("ejemplo2");
            datos.Add("ejemplo3");

            Byte[] excel = ExcelGenerator.GetExcel<string>(datos, "Prueba");

            Assert.IsNotNull(excel);
        }
    }
}
