﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Utilities
{
    public class ExcelGenerator
    {
        public static Byte[] GetExcel<T>(IEnumerable<T> elements, string nombre = "Reporte")
        {
            Byte[] fileContents = null;

            try
            {
                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add(nombre);

                    worksheet.Cells.LoadFromCollection<T>(elements, true);

                    int max = typeof(T).GetProperties().Length;

                    for (int i = 1; i <= max; i++)
                    {
                        worksheet.Cells[1, i].Style.Font.Size = 12;
                        worksheet.Cells[1, i].Style.Font.Bold = true;
                        worksheet.Cells[1, i].Style.Font.Color.SetColor(System.Drawing.Color.White);
                        worksheet.Cells[1, i].Style.Border.Top.Style = ExcelBorderStyle.Hair;
                        worksheet.Cells[1, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[1, i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(68, 114, 196));
                        worksheet.Column(i).AutoFit();
                    }
                    
                    ExcelRange range = worksheet.Cells[1, 1, elements.Count(), max];

                    OfficeOpenXml.Table.ExcelTable table = worksheet.Tables.Add(range, nombre.Replace(" ", String.Empty));

                    table.ShowHeader = true;
                    table.TableStyle = OfficeOpenXml.Table.TableStyles.Medium2;

                    fileContents = package.GetAsByteArray();
                }
            }
            catch (Exception ex) { throw ex; }

            return fileContents;
        }
    }
}
